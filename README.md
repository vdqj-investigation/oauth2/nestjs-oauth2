# NESTJS - OAUTH2 - KEYCLOAK

## Descripcion

Proyecto hecho con nestjs para conectarse a keycloak a travez de OAuth2

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Requerimientos:

- Proyecto hecho con Nestjs
- Node version 18.16.0

## Ramas:

- main: rama con proyecto base en blanco
- 

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
